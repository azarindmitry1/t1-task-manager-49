package ru.t1.azarin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.azarin.tm.dto.response.system.ApplicationAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public final static String NAME = "about";

    @NotNull
    public final static String ARGUMENT = "-a";

    @NotNull
    public final static String DESCRIPTION = "Display developer info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = getSystemEndpoint().getAbout(request);
        System.out.println(response.getName());
        System.out.println(response.getEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
