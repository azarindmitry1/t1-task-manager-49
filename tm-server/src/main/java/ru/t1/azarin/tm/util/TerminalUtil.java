package ru.t1.azarin.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextInteger() {
        @NotNull final String value = SCANNER.nextLine();
        return Integer.parseInt(value);
    }

}
