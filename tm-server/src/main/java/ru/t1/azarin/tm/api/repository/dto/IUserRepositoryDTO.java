package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    void clear();

    @Nullable
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@NotNull String id);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    void removeById(@NotNull String id);

}
