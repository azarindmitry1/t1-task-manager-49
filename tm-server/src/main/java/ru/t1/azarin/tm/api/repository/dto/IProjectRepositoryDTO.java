package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.ProjectDTO;
import ru.t1.azarin.tm.enumerated.Sort;

import java.util.List;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findAll(@NotNull String userId, @NotNull Sort sort);

}