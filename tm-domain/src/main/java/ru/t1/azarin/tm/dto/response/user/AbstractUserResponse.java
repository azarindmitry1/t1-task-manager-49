package ru.t1.azarin.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.UserDTO;
import ru.t1.azarin.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    public AbstractUserResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

}
